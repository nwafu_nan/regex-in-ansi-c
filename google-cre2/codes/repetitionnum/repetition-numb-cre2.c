#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<cre2.h>

// 排序比较函数
int cmp(const void *p, const void *q);
// 输出重复数字
void print_repetition(int n);

int main()
{
    int i;
    int a[] = {282112, 2822, 1234, 112355, 12345678};

    for(i = 0; i < sizeof(a) / sizeof(a[0]); i++)
    {
        printf("%d: ", a[i]);
        print_repetition(a[i]);
    }

    return 0;
}

// 输出重复数字
void print_repetition(int n)
{
    int flag = 0;
    // 数字字符串缓存
    char s[8] = {0};
    const char *pat;
    cre2_string_t input;
    int c;
    int nmatch = 1;
    cre2_string_t match;

    // 判断整数有效性
    if(n <= 0 || n > 10000000)
    {
        printf("Invalid input\n");
        return;
    }

    if(n > 0 && n < 10)
    {
        printf("No repeated numbers.\n");
        return;
    }

    // 将整数转换为字符串并排序
    sprintf(s, "%d", n);
    qsort(s, strlen(s), 1, cmp);

    // 正则表达式(RE2不支持\1这样的反向数字引用)
    // 对整体使用小括号以构成group 0
    pat = "(0{2,}|1{2,}|2{2,}|3{2,}|4{2,}|5{2,}|6{2,}|7{2,}|8{2,}|9{2,})";
    input.data = s;
    input.length = strlen(s);

    c = cre2_find_and_consume(pat, &input, &match, nmatch);
    while(c == 1)
    {
        putchar(match.data[0]);
        putchar(' ');

        // 置标志
        flag = 1;

        c = cre2_find_and_consume(pat, &input, &match, nmatch);
    }

    // 无重复数字
    if(!flag)
    {
        printf("No repeated numbers.\n");
    }
    else
    {
        printf("\n");
    }
}

int cmp(const void *p, const void *q)
{
    char ch_p = *((char*)p);
    char ch_q = *((char*)q);

    return (ch_p > ch_q) - (ch_p < ch_q);
}
