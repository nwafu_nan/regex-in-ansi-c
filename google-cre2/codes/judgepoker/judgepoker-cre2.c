#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<cre2.h>

// 函数声明
int judge_poker(const char *in_hand, const char *op_type);

// 测试/驱动
int main()
{
    int i;
    char *in[] = {"12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "66666666666666", "22222222222222", "12356789",
                  "1111111111123456789"};
    char *op[] = {"33", "8", "444", "88", "6", "777", "12345",
                  "45678", "1111", "12345", "12345", "12345", "12345"};

    for(i = 0; i < sizeof(in) / sizeof(in[0]); i++)
    {
        if(judge_poker(in[i], op[i]))
        {
            puts("YES");
        }
        else
        {
            puts("NO");
        }
    }

    return 0;
}

int judge_poker(const char *in_hand, const char *op_type)
{
    // 正则表达式查找表
    const char *re_pat[] = {
      "(2{1})|(3{1})|(4{1})|(5{1})|(6{1})|(7{1})|(8{1})|(9{1})", // 2--9
      "(2{2})|(3{2})|(4{2})|(5{2})|(6{2})|(7{2})|(8{2})|(9{2})", // 22等
      "(2{3})|(3{3})|(4{3})|(5{3})|(6{3})|(7{3})|(8{3})|(9{3})", // 222等
      "(2{4})|(3{4})|(4{4})|(5{4})|(6{4})|(7{4})|(8{4})|(9{4})", // 2222等
      "(2+3+4+5+6)|" // 23456
      "(3+4+5+6+7)|" // 34567
      "(4+5+6+7+8)|" // 45678
      "(5+6+7+8+9)"  // 56789
    };

    const char *pat;

    // 正则对象指针
    cre2_regexp_t *rex;
    // 选项对象指针
    cre2_options_t *opt;
    int c;

    size_t op_len = strlen(op_type);
    size_t off_ratio;

    // 根据对手牌长度确定正则表达式字符串偏移量系数
    off_ratio = op_len == 5 ? 12 : 7;

    // 根据对手牌型取得正则表达式字符串
    pat = re_pat[op_len - 1] + (op_type[0] - '0' - 1) * off_ratio;

    size_t in_len = strlen(in_hand);

    // 创建选项对象
    opt = cre2_opt_new();

    // 编译正则表达式，构建正则对象
    rex = cre2_new(pat, strlen(pat), opt);
    if(cre2_error_code(rex))
    {
        exit(EXIT_FAILURE);
    }

    // 执行匹配
    c = cre2_match(rex, in_hand, in_len, 0, in_len,
                   CRE2_UNANCHORED, NULL, 0);

    // 释放空间
    cre2_delete(rex);
    cre2_opt_delete(opt);

    // 返回结果
    return c;
}

