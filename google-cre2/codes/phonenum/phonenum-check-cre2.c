#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<cre2.h>

// 函数原型
int check_phone_num(const char * num);

int main()
{
    int i;
    char *s[] = {"029-849-1249", "1-885-1202", "876-9182",
                 "1923-989-2912", "0591-089-2126", "021-29-8932",
                 "9280-9981", "910-92817", "01-9A9-8712",
                 "298491249", "0086-010-102-9182",
                 "A09-021-928-1921", "123-1211", "29-1+983"};

    for(i = 0; i < sizeof(s) / sizeof(s[0]); i++)
    {
        if(check_phone_num(s[i]))
        {
            printf("%s is valid phonenum.\n", s[i]);
        }
        else
        {
            printf("%s is not valid phonenum.\n", s[i]);
        }
    }

    return 0;
}

// 判断电话号码有效性
int check_phone_num(const char * num)
{
    int c;
    const char *pat = "^(([0-9]{0,4})-)?[1-9][0-9]{2}-[0-9]{4}$";

    c = cre2_easy_match(pat, strlen(pat), num, strlen(num), NULL, 0);
    if(c == 2)
    {
        exit(EXIT_FAILURE);
    }

    // 返回结果
    return c;
}
