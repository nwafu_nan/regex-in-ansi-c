#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<cre2.h>

int is_valid_int(const char *s, const char *pat);

int main()
{
    int i;
    char *s[] = {"7", "123", "-1234", "1000", "12345"};
    // 判断是否为[1, 1000]内的整数的正则表达式
    const char *pat = "^[1-9][0-9]{0,2}$|^1000$";

    for(i = 0; i < sizeof(s) / sizeof(s[0]); i++)
    {
        if(is_valid_int(s[i], pat))
        {
            printf("%s is valid int.\n", s[i]);
        }
        else
        {
            printf("%s is not valid int.\n", s[i]);
        }
    }

    return 0;
}

// 判断字符串是否为有效的整数
int is_valid_int(const char *s, const char *pat)
{
    // 正则对象指针
    cre2_regexp_t *rex;
    // 选项对象指针
    cre2_options_t *opt;
    int c;

    size_t s_len = strlen(s);

    // 创建选项对象
    opt = cre2_opt_new();
    // 设置选项(启用POSIX语法)
    cre2_opt_set_posix_syntax(opt, 1);

    // 编译正则表达式，构建正则对象
    rex = cre2_new(pat, strlen(pat), opt);
    if(cre2_error_code(rex))
    {
        exit(EXIT_FAILURE);
    }

    // 执行匹配
    c = cre2_match(rex, s, s_len, 0, s_len,
                   CRE2_UNANCHORED, NULL, 0);

    // 释放空间
    cre2_delete(rex);
    cre2_opt_delete(opt);

    // 返回结果
    return c;
}
