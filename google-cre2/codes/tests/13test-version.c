#include <stdio.h>
#include <stdlib.h>
#include <cre2.h>

int main (void)
{
    printf("version number string: %s\n", cre2_version_string());
    printf("libtool version number: %d:%d:%d\n",
            cre2_version_interface_current(),
            cre2_version_interface_revision(),
            cre2_version_interface_age());

    exit(EXIT_SUCCESS);
}
