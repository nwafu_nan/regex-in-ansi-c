// 8位宽字符
#define PCRE2_CODE_UNIT_WIDTH 8

#include<stdio.h>
#include<string.h>
#include<pcre2.h>

// 函数声明
int judge_poker(const char *in_hand, const char *op_type);

// 测试/驱动
int main()
{
    int i;
    char *in[] = {"12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "66666666666666", "22222222222222", "12356789",
                  "1111111111123456789"};
    char *op[] = {"33", "8", "444", "88", "6", "777", "12345",
                  "45678", "1111", "12345", "12345", "12345", "12345"};

    for(i = 0; i < sizeof(in) / sizeof(in[0]); i++)
    {
        if(judge_poker(in[i], op[i]))
        {
            puts("YES");
        }
        else
        {
            puts("NO");
        }
    }

    return 0;
}

int judge_poker(const char *in_hand, const char *op_type)
{
    // 正则处理相关变量
    pcre2_code *re;
    int rc;
    int errornumber;
    PCRE2_SIZE erroroffset;
    pcre2_match_data *match_data;
    PCRE2_SPTR pattern;
    PCRE2_SPTR subject;

    // 正则表达式查找表
    const char *re_pat[] = {
      "(2{1})|(3{1})|(4{1})|(5{1})|(6{1})|(7{1})|(8{1})|(9{1})", // 2--9
      "(2{2})|(3{2})|(4{2})|(5{2})|(6{2})|(7{2})|(8{2})|(9{2})", // 22等
      "(2{3})|(3{3})|(4{3})|(5{3})|(6{3})|(7{3})|(8{3})|(9{3})", // 222等
      "(2{4})|(3{4})|(4{4})|(5{4})|(6{4})|(7{4})|(8{4})|(9{4})", // 2222等
      "(2+3+4+5+6)|" // 23456
      "(3+4+5+6+7)|" // 34567
      "(4+5+6+7+8)|" // 45678
      "(5+6+7+8+9)"  // 56789
    };

    size_t op_len = strlen(op_type);
    size_t off_ratio;

    // 根据对手牌长度确定正则表达式字符串偏移量系数
    off_ratio = op_len == 5 ? 12 : 7;

    // 根据对手牌型取得正则表达式字符串
    pattern = (PCRE2_SPTR)(re_pat[op_len - 1] +
              (op_type[0] - '0' - 1) * off_ratio);
    subject = (PCRE2_SPTR)in_hand;

    // 编译正则表达式
    re = pcre2_compile(pattern,               /* 模式(正则表达式) */
                       PCRE2_ZERO_TERMINATED, /* 模式长度('\0'终止字符串) */
                       0,                     /* 默认选项 */
                       &errornumber,          /* 错误编码 */
                       &erroroffset,          /* 错误信息偏移量 */
                       NULL);                 /* 上下文，一般取NULL */
    /* 编译失败处理. */
    if (re == NULL)
    {
        PCRE2_UCHAR buffer[256];
        pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
        printf("PCRE2 compilation failed at offset %d: %s\n",
               (int)erroroffset, buffer);
        exit(EXIT_FAILURE);
    }

    // 构造匹配寄存器以记录匹配结果
    match_data = pcre2_match_data_create_from_pattern(re, NULL);
    // 匹配
    rc = pcre2_match(re, subject, PCRE2_ZERO_TERMINATED,
                     0, 0, match_data, NULL);

    // 释放内存
    pcre2_match_data_free(match_data);
    pcre2_code_free(re);

    // 返回结果
    return rc >= 0 ? 1 : 0;
}

