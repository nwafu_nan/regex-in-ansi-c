#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<hs/hs.h>

// 匹配回调函数
static int on_match(unsigned int id, unsigned long long from,
        unsigned long long to, unsigned int flags, void *ctx);
void is_valid_int(const char *s, const char *pat, int *flag);

int main()
{
    int i;
    char *s[] = {"7", "123", "-1234", "1000", "12345"};
    // 判断是否为[1, 1000]内的整数的正则表达式
    const char *pat = "^[1-9][0-9]{0,2}$|^1000$";
    int flag;

    for(i = 0; i < sizeof(s) / sizeof(s[0]); i++)
    {
        flag = 0;
        is_valid_int(s[i], pat, &flag);
        if(flag)
        {
            printf("%s is valid int.\n", s[i]);
        }
        else
        {
            printf("%s is not valid int.\n", s[i]);
        }
    }

    return 0;
}

// 判断字符串是否为有效的整数
void is_valid_int(const char *s, const char *pat, int *flag)
{
    // 正则对象指针
    hs_database_t *db = NULL;
    hs_compile_error_t *compile_err = NULL;

    // 编译正则表达式
    if(hs_compile(pat, HS_FLAG_DOTALL, HS_MODE_BLOCK, NULL,
                  &db, &compile_err) != HS_SUCCESS)
    {
        fprintf(stderr, "ERROR: Unable to compile pattern \"%s\": %s\n",
                pat, compile_err->message);
        hs_free_compile_error(compile_err);
        exit(EXIT_FAILURE);
    }

    // 申请临时空间
    hs_scratch_t *scratch = NULL;
    if(hs_alloc_scratch(db, &scratch) != HS_SUCCESS)
    {
        fprintf(stderr, "ERROR: Unable to allocate scratch space."
                        " Exiting.\n");
        hs_free_database(db);
        exit(EXIT_FAILURE);
    }

    // 正则匹配
    if(hs_scan(db, s, strlen(s), 0, scratch, on_match, flag) != HS_SUCCESS)
    {
        fprintf(stderr, "ERROR: Unable to scan input buffer. Exiting.\n");
        hs_free_scratch(scratch);
        hs_free_database(db);
        exit(EXIT_FAILURE);
    }

    // 释放空间
    hs_free_scratch(scratch);
    hs_free_database(db);
}

static int on_match(unsigned int id, unsigned long long from,
        unsigned long long to, unsigned int flags, void *ctx)
{
    int *flag = (int *)ctx;

    *flag = 1;

    return 0; // 返回0继续匹配，返回1结束匹配
}
