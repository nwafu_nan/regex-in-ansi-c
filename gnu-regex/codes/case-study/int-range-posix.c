#include<stdio.h>
#include<string.h>
#include<regex.h>

int is_valid_int(const char *s, const char *pat);

int main()
{
    int i;
    char *s[] = {"7", "123", "-1234", "1000", "12345"};
    // 判断是否为[1, 1000]内的整数的正则表达式
    const char *pat = "^[1-9][0-9]{0,2}$|^1000$";

    for(i = 0; i < sizeof(s) / sizeof(s[0]); i++)
    {
        if(is_valid_int(s[i], pat))
        {
            printf("%s is valid int.\n", s[i]);
        }
        else
        {
            printf("%s is not valid int.\n", s[i]);
        }
    }

    return 0;
}

// 判断字符串是否为有效的整数
int is_valid_int(const char *s, const char *pat)
{
    int c;
    regex_t pat_buf;

    // 编译正则表达式
    c = regcomp(&pat_buf, pat, REG_NEWLINE | REG_EXTENDED);
    if(c != 0)
    {
        return 0;
    }

    // 正则匹配
    c = regexec(&pat_buf, s, 0, NULL, 0);
    regfree(&pat_buf);

    // 不是[1, 1000]之间的整数
    return c != 0 ? 0 : 1;
}
