#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

// 函数声明
int judge_poker(const char *in_hand, const char *op_type);

// 测试/驱动
int main()
{
    int i;
    char *in[] = {"12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "66666666666666", "22222222222222", "12356789",
                  "1111111111123456789"};
    char *op[] = {"33", "8", "444", "88", "6", "777", "12345",
                  "45678", "1111", "12345", "12345", "12345", "12345"};

    for(i = 0; i < sizeof(in) / sizeof(in[0]); i++)
    {
        if(judge_poker(in[i], op[i]))
        {
            puts("YES");
        }
        else
        {
            puts("NO");
        }
    }

    return 0;
}

int judge_poker(const char *in_hand, const char *op_type)
{
    // 声明struct re_pattern_buffer对象
    regex_t pat_buf;
    char err_buf[256];
    int cflags, eflag;
    int c;
    const char *pat = NULL;

    // 正则表达式查找表
    char *re_pat[] = {
      "(2{1})|(3{1})|(4{1})|(5{1})|(6{1})|(7{1})|(8{1})|(9{1})", // 2--9
      "(2{2})|(3{2})|(4{2})|(5{2})|(6{2})|(7{2})|(8{2})|(9{2})", // 22等
      "(2{3})|(3{3})|(4{3})|(5{3})|(6{3})|(7{3})|(8{3})|(9{3})", // 222等
      "(2{4})|(3{4})|(4{4})|(5{4})|(6{4})|(7{4})|(8{4})|(9{4})", // 2222等
      "(2+3+4+5+6)|" // 23456
      "(3+4+5+6+7)|" // 34567
      "(4+5+6+7+8)|" // 45678
      "(5+6+7+8+9)"  // 56789
    };

    size_t op_len = strlen(op_type);
    size_t off_ratio;

    // 根据对手牌长度确定正则表达式字符串偏移量系数
    off_ratio = op_len == 5 ? 12 : 7;

    // 根据对手牌型取得正则表达式字符串
    pat = re_pat[op_len - 1] + (op_type[0] - '0' - 1) * off_ratio;

    // 设置正则表达式语法
    cflags = REG_EXTENDED;

    // 编译正则表达式
    c = regcomp(&pat_buf, pat, cflags);
    // 正则表达式编译出错
    if(c != 0)
    {
        regerror(c, &pat_buf, err_buf, sizeof(err_buf));
        err_buf[sizeof(err_buf) - 1] = '\0';
        printf("%s\n", err_buf);
        exit(EXIT_FAILURE);
    }

    // 不执行查寻功能
    eflag = 0;

    // 正则匹配("0"表示匹配成功，"非0"表示匹配错误)
    c = regexec(&pat_buf, in_hand, 0, NULL, eflag);

    // 释放pattern buffer
    regfree(&pat_buf);

    // 返回结果
    return c == 0 ? 1 : 0;
}

