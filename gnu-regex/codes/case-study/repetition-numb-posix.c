#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

// 排序比较函数
int cmp(const void *p, const void *q);
// 输出重复数字
void print_repetition(int n);

int main()
{
    int i;
    int a[] = {282112, 2822, 1234, 112355, 12345678};

    for(i = 0; i < sizeof(a) / sizeof(a[0]); i++)
    {
        printf("%d: ", a[i]);
        print_repetition(a[i]);
    }

    return 0;
}

// 输出重复数字
void print_repetition(int n)
{
    char s[8] = {0};
    const char *p_str;
    size_t offset = 0;
    int flag = 0;

    // 记录整体匹配数据
    const size_t nmatchs = 1;
    regmatch_t pmatch[1];
    char err_buf[256];
    regex_t pat_buf;
    int c;

    // 正则表达式(注意用\\转义)
    const char pat[] = "([0-9])\\1+";

    // 判断整数有效性
    if(n <= 0 || n > 10000000)
    {
        printf("Invalid input\n");
        return;
    }

    if(n > 0 && n < 10)
    {
        printf("No repeated numbers.\n");
        return;
    }

    // 将整数转换为字符串并排序
    sprintf(s, "%d", n);
    qsort(s, strlen(s), 1, cmp);


    // 编译正则表达式
    c  = regcomp(&pat_buf, pat, REG_EXTENDED);
    if(c != 0)
    {
        regerror(c, &pat_buf, err_buf, sizeof(err_buf));
        err_buf[sizeof(err_buf) - 1] = '\0';
        printf("%s\n", err_buf);
        return;
    }

    // 变量赋初值
    offset = 0;
    flag = 0;

    // 循环正则搜索
    do
    {
        // 字符串偏移
        p_str = s + offset;

        // 正则搜索
        c = regexec(&pat_buf, p_str, nmatchs, pmatch, 0);

        if(c != 0) // 未找到，退出循环
        {
            break;
        }
        else
        {
            // 输出重复数字
            putchar(p_str[pmatch[0].rm_so]);
            putchar(' ');

            // 置标志
            flag = 1;

            // 下一次查找开始位置
            offset += pmatch[0].rm_eo;
            continue;
        }
    }while(1);

    // 释放空间
    regfree(&pat_buf);

    // 无重复数字
    if(!flag)
    {
        printf("No repeated numbers.\n");
    }
    else
    {
        printf("\n");
    }
}

int cmp(const void *p, const void *q)
{
    char ch_p = *((char*)p);
    char ch_q = *((char*)q);

    return (ch_p > ch_q) - (ch_p < ch_q);
}
