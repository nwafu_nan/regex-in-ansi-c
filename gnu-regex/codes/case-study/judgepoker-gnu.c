#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

// 函数声明
int judge_poker(const char *in_hand, const char *op_type);

// 测试/驱动
int main()
{
    int i;
    char *in[] = {"12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "12233445566677", "12233445566677", "12233445566677",
                  "66666666666666", "22222222222222", "12356789",
                  "1111111111123456789"};
    char *op[] = {"33", "8", "444", "88", "6", "777", "12345",
                  "45678", "1111", "12345", "12345", "12345", "12345"};

    for(i = 0; i < sizeof(in) / sizeof(in[0]); i++)
    {
        if(judge_poker(in[i], op[i]))
        {
            puts("YES");
        }
        else
        {
            puts("NO");
        }
    }

    return 0;
}

int judge_poker(const char *in_hand, const char *op_type)
{
    // 声明struct re_pattern_buffer对象
    struct re_pattern_buffer pat_buf;
    const char *err_code;
    int pos;
    const char *pat = NULL;

    // 正则表达式查找表
    char *re_pat[] = {
      "(2{1})|(3{1})|(4{1})|(5{1})|(6{1})|(7{1})|(8{1})|(9{1})", // 2--9
      "(2{2})|(3{2})|(4{2})|(5{2})|(6{2})|(7{2})|(8{2})|(9{2})", // 22等
      "(2{3})|(3{3})|(4{3})|(5{3})|(6{3})|(7{3})|(8{3})|(9{3})", // 222等
      "(2{4})|(3{4})|(4{4})|(5{4})|(6{4})|(7{4})|(8{4})|(9{4})", // 2222等
      "(2+3+4+5+6)|" // 23456
      "(3+4+5+6+7)|" // 34567
      "(4+5+6+7+8)|" // 45678
      "(5+6+7+8+9)"  // 56789
    };

    size_t op_len = strlen(op_type);
    size_t off_ratio;

    // 根据对手牌长度确定正则表达式字符串偏移量系数
    off_ratio = op_len == 5 ? 12 : 7;

    // 根据对手牌型取得正则表达式字符串
    pat = re_pat[op_len - 1] + (op_type[0] - '0' - 1) * off_ratio;

    // 初始化
    memset(&pat_buf, 0, sizeof(struct re_pattern_buffer));
    re_set_syntax(RE_SYNTAX_EGREP);

    // 编译正则表达式
    err_code  = re_compile_pattern(pat, strlen(pat), &pat_buf);
    if(err_code != NULL)
    {
        printf("error on compiling regex.code = %s\n", err_code);
        exit(EXIT_FAILURE);
    }

    // 用编译后的正则表达式实现查找
    pos = re_search(&pat_buf, in_hand, strlen(in_hand),
                    0, strlen(in_hand), NULL);

    // 释放pattern buffer
    regfree(&pat_buf);

    // 返回结果
    return pos >= 0 ? 1 : 0;
}

