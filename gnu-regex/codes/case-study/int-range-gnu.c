#define _GNU_SOURCE

#include<stdio.h>
#include<string.h>
#include<regex.h>

int is_valid_int(const char *s, const char *pat);

int main()
{
    int i;
    char *s[] = {"7", "123", "-1234", "1000", "12345"};
    // 判断是否为[1, 1000]内的整数的正则表达式
    const char *pat = "^[1-9][0-9]{0,2}$|^1000$";

    for(i = 0; i < sizeof(s) / sizeof(s[0]); i++)
    {
        if(is_valid_int(s[i], pat))
        {
            printf("%s is valid int.\n", s[i]);
        }
        else
        {
            printf("%s is not valid int.\n", s[i]);
        }
    }

    return 0;
}

// 判断字符串是否为有效的整数
int is_valid_int(const char *s, const char *pat)
{
    // 声明struct re_pattern_buffer对象
    struct re_pattern_buffer pat_buf;
    const char *err_code;
    int match_ret;

    memset(&pat_buf, 0, sizeof(struct re_pattern_buffer));
    re_set_syntax(RE_SYNTAX_EGREP);

    // 编译正则表达式
    err_code  = re_compile_pattern(pat, strlen(pat), &pat_buf);
    if(err_code != NULL)
    {
        printf("erro: %s\n", err_code);
        regfree(&pat_buf);
        return 0;
    }

    // 匹配检查
    match_ret = re_match(&pat_buf, s, strlen(s), 0, NULL);

    // 释放空间
    regfree(&pat_buf);

    // 返回结果
    retrun match_ret >= 0 ? 1 : 0;
}

