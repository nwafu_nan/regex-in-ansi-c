// 使用GNU用户界面
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 正则表达式库
#include <regex.h>

// 输出宏
#define PRINT_INT(n) printf(#n " = %d\n", n)
#define PRINT_LD(n) printf(#n " = %ld\n", n)
#define PRINT_STR(s) printf(#s " = %s\n", s)

int main()
{
    // 声明struct re_pattern_buffer对象
    struct re_pattern_buffer pat_buf;
    // 记录匹配信息的结构体变量(匹配寄存器)
    struct re_registers regs;
    const char *err_code;
    int match_ret, i;

    // 正则表达式字符串
    char pat_str[] = "1[^3]*3";

    // 待匹配字符串
    char* str[] = {"13", "31", "7778999", "10003"};

    // 必须设置以下4个字段
    pat_buf.translate = 0;
    pat_buf.fastmap = 0;
    // 以下两个字段必须设置为0，以让库实现动态内存分配
    pat_buf.buffer = 0;
    pat_buf.allocated = 0;

    // 设置正则语法的全局变量
    re_syntax_options = RE_SYNTAX_EGREP;

    // 编译正则表达式
    err_code = re_compile_pattern(pat_str, strlen(pat_str), &pat_buf);

    if(err_code != NULL)
    {
        printf("erro: %s\n", err_code);
        regfree(&pat_buf);
        exit(EXIT_FAILURE);
    }

    for(i = 0; i < sizeof(str) / sizeof(char*); i++)
    {

        // 返回匹配字长长度(有可能是0，但表示有匹配)
        match_ret = re_match(&pat_buf, str[i], strlen(str[i]), 0, &regs);

        // 匹配结果
        PRINT_INT(match_ret);

        if(match_ret >= 0) // 匹配成功
        {
            printf("unlucky\n");
            PRINT_INT(regs.start[0]); // 输出整体匹配起始位置
            PRINT_INT(regs.end[0]);   // 输出整体匹配结束位置
        }
        else if(match_ret == -1) // 没有匹配
        {
            printf("lucky\n");
        }
        else // 其它值表示有错误发生
        {
            perror("re_match");
        }
        printf("\n");
    }

    // 释放空间
    regfree(&pat_buf);
    // 匹配寄存器成员需要单独释放
    free(regs.start);
    free(regs.end);

    return 0;
}
