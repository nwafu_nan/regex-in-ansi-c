#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

// 输出字符串中指定范围内的字符
void print_substr(const char* s, size_t __start, size_t __end);

int main (int argc, char** argv)
{
    // 正则缓存
    regex_t pat_buf;
    // 匹配寄存器
    const size_t nmatchs = 1;
    regmatch_t pmatch[1];
    char err_buf[256];
    const char *p_str = NULL;
    size_t offset = 0;
    int c;

    // 正则表达式
    char* pat_str = "([0-9]+) (apples|oranges)";
    // 被匹配字符串
    char* str = "I have 20 apples and 34 oranges.";

    // 编译正则表达式
    c = regcomp(&pat_buf, pat_str, REG_EXTENDED);
    if(c != 0)
    {
        regerror(c, &pat_buf, err_buf, sizeof(err_buf));
        // 确保最后一个字符为'\0'
        err_buf[sizeof(err_buf) - 1] = '\0';
        printf("%s\n", err_buf);
        regfree(&pat_buf);
        exit(EXIT_FAILURE);
    }

    // 变量赋初值
    offset = 0;
    // 循环正则搜索
    do
    {
        // 字符串偏移
        p_str = str + offset;
        /* puts(p_str); */

        // 正则搜索
        c = regexec(&pat_buf, p_str, nmatchs, pmatch, 0);

        if(c != 0) // 未找到，退出循环
        {
            break;
        }
        else
        {
            print_substr(p_str, pmatch[0].rm_so, pmatch[0].rm_eo);
            // 下一次查找开始位置
            offset += pmatch[0].rm_eo;
            continue;
        }
    }while(1);

    // 释放空间
    regfree(&pat_buf);

    return 0;
}

/* 输出字符串中指定范围的字符 */
void print_substr(const char* s, size_t __start, size_t __end)
{
    int i;

    if(s == NULL)
    {
        return;
    }

    for(i = __start; i < __end; ++i)
    {
        putchar(s[i]);
    }

    putchar('\n');
}
