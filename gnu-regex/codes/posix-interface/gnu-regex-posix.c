#include <stdio.h>
#include <stdlib.h>
#include <regex.h>

// 输出宏
#define PRINT_INT(n) printf(#n " = %d\n", n)
#define PRINT_LD(n) printf(#n " = %ld\n", n)
#define PRINT_STR(s) printf(#s " = %s\n", s)

// 输出字符串中指定范围内的字符
void print_substr(const char* s, size_t __start, size_t __end);

int main()
{
    int id, n, i;
    // 错误信息字符串
    char err_buf[256];
    // 正则缓存
    regex_t pat_buf;

    // 正则表达式
    const char* pat_str = "(we|par)([a-z]+)";
    // 待匹配字符串
    const char* str = "hello,welcome to my party";

    // 记录分组匹配数据(匹配寄存器)
    const size_t nmatch = 3; // 有2个分组，加上默认组(group0)，共3个
    regmatch_t pmatch[3];

    // 编译正则表达式
    id = regcomp(&pat_buf, pat_str, REG_EXTENDED);
    if(id != 0)
    {
        regerror(id, &pat_buf, err_buf, sizeof(err_buf));
        err_buf[sizeof(err_buf) - 1] = '\0'; // 确保最后一个字符是'\0'
        PRINT_STR(err_buf);

        exit(EXIT_FAILURE);
    }

    // 正则匹配，返回0表示匹配成功
    n = regexec(&pat_buf, str, nmatch, pmatch, 0);

    if(n == REG_NOMATCH)
    {
        // 没有找到匹配 */
        printf("can't match!\n");
    }
    else if(n == 0)
    {
        // 找到匹配，输出
        for (i = 0; i < nmatch; i++)
        {
            printf("group %d : [", i);
            printf("%d, %d", pmatch[i].rm_so, pmatch[i].rm_eo);
            printf(")-->");
            print_substr(str, pmatch[i].rm_so, pmatch[i].rm_eo);
        }
    }
    else
    {
        // regexec 调用出错
        regerror(n, &pat_buf, err_buf, sizeof(err_buf));
        err_buf[sizeof(err_buf) - 1] = '\0';
        printf("%s\n", err_buf);
    }

    // 释放空间
    regfree(&pat_buf);

    return 0;
}

/* 输出字符串中指定范围的字符 */
void print_substr(const char* s, size_t __start, size_t __end)
{
    int i;

    if(s)
    {
        for(i = __start; i < __end; ++i)
        {
            putchar(s[i]);
        }
    }

    putchar('\n');
}
