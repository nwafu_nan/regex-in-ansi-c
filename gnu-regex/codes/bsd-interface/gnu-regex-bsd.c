// 使用BSD用户界面
#define _GNU_SOURCE
#define _REGEX_RE_COMP

#include <stdio.h>
#include <stdlib.h>
#include <regex.h>

int main()
{
    int n;
    const char *id;

    /* 正则表达式 */
    const char* pat_str = "(we|par)([a-z]+)";
    /* 待匹配字符串 */
    const char* str = "hello,welcome to my party";

    // 设置正则语法的全局变量
    re_syntax_options = RE_SYNTAX_EGREP;

    // 编译正则表达式
    id = re_comp(pat_str);
    if(id != NULL)
    {
        exit(EXIT_FAILURE);
    }

    // 匹配
    n = re_exec(str);

    if(n == 1)
    {
        printf("re_exec match string: %s\n", str);
    }

    return 0;
}
